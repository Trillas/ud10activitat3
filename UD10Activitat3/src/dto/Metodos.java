package dto;

import exceptions.excepciones;

public class Metodos {

	//Un metodo para generar un n�mero aleatorio
	public static int numAleatorio(int min, int max) {
		int num;
		num = (int)(Math.random() * ((max - min) + 1) + min);
		return num;
	}
	
	
	//Para ejecutar
	public static void hacerPartida() {
		int numGenerado = numAleatorio(0, 999);
		System.out.println("El n�mero generado es: " + numGenerado);
        try {
            if (numGenerado % 2 == 0) {
                throw new excepciones(numGenerado);
            } else {
                throw new excepciones(numGenerado);
            }
        } catch (excepciones ej) {
            System.out.println(ej.getMessage());
        }
	}

}
